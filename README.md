WoPlus.SDK.Spring
===============

在Spring中使用SDK的方法

###第一步
在applicationContext.xml中声明bean

    <bean id="woplusClient" class="cn.chinaunicom.woplus.openapi.spring.WoPlusClient">
      <constructor-arg index="0" value="[这里填写你的AppKey]" />
	  <constructor-arg index="1" value="[这里填写你的AppSecret]" />
    </bean>
	
###第二步
在需要使用的地方，引入这个Bean即可。

    @Autowired
    WoPlusClient woplusClient;
	
###第三步
现在你可以方便地使用woplusClient了，示例如下：

      @RequestMapping(value="/api/paymentcodesms",method=RequestMethod.GET,headers = {"content-type=application/json;charset=utf-8", "Accept=application/json"})
	  public @ResponseBody WoPlusResponse paymentcodesms(HttpServletRequest request,@RequestParam("mobile")String mobile){
		
		WoPlusResponse resp = new WoPlusResponse();
		
		String api_url="https://open.wo.com.cn/openapi/rpc/paymentcodesms/v2.0";
		
		HashMap<String,Object> params = new HashMap<String,Object>();
		
		long num=new Random().nextLong();
		params.put("paymentUser", mobile);
		params.put("paymentType", 0);
		params.put("outTradeNo",Long.toString(num));
		params.put("paymentAcount", "001");
		params.put("subject", "金币一堆");
		params.put("totalFee", 0.01f);
		
		try {
			resp = woplusClient.post(api_url, params,false);
			if(resp.resultCode.equals("0")){
				request.getSession().setAttribute("paymentcodesms_param", params);
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resp;
	}
	
	
###大功告成
	